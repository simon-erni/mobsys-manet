package ch.hslu.mobsys.manet;

import ch.hslu.mobsys.manet.protocol.ManetMessage;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class ManetMessageTest {

    private ManetMessage message;

    @Before
    public void setUp() throws Exception {
        this.message = new ManetMessage();
    }

    @Test
    public void testDifferentValues() throws Exception {
        ManetMessage message1 = new ManetMessage();
        ManetMessage message2 = new ManetMessage();


        message1.setuId(0);
        message1.setIdentifier("hallo");
        message1.setMessage("hai");

        byte[] message1Bytes = message1.getBytes();

        message2.setuId(1);
        message2.setIdentifier("hallo");
        message2.setMessage("hai");

        byte[] message2Bytes = message2.getBytes();

        assertThat(message1Bytes, not(equalTo(message2Bytes)));

    }

    @Test
    public void testSameValues() throws Exception {
        ManetMessage message1 = new ManetMessage();
        ManetMessage message2 = new ManetMessage();


        message1.setuId(0);
        message1.setIdentifier("hallo");
        message1.setMessage("hai");

        byte[] message1Bytes = message1.getBytes();

        message2.setuId(0);
        message2.setIdentifier("hallo");
        message2.setMessage("hai");

        byte[] message2Bytes = message2.getBytes();

        assertThat(message1Bytes, equalTo(message2Bytes));

    }

    @Test
    public void testByteMessageLength() throws Exception {

        assertThat(message.getBytes().length, is(174));
    }


    @Test
    public void testUIDMaxLength() {
        message.setuId(Integer.MAX_VALUE);


        message.getBytes();
    }

    @Test
    public void testMessageFieldMaxLength() throws Exception {

        message.setMessage("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

        message.getBytes();
    }




    public void testTooMuchLengthIdentifier() throws Exception {
        message.setIdentifier("12345678901");

        message.getBytes();

    }


    public void testTooMuchLengthMessage() throws Exception {
        message.setuId(0);
        message.setIdentifier("1234567890");
        message.setMessage("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901");

        message.getBytes();
    }


    @Test
    public void testUIDConverting() throws Exception {
        message.setuId(1337);

        byte[] bytes = message.getBytes();

        ManetMessage newMessage = new ManetMessage(bytes);

        assertTrue(message.getuId() == newMessage.getuId());

    }

    @Test
    public void testIdentifierConverting() throws Exception {

        message.setIdentifier("123456789");

        byte[] bytes = message.getBytes();

        ManetMessage newMessage = new ManetMessage(bytes);


        String oldIdentifier = message.getIdentifier();
        String newIdentifier = newMessage.getIdentifier();


        assertTrue(oldIdentifier.equals(newIdentifier));

    }

    @Test
    public void testMessageConverting() throws Exception {

        message.setMessage("HalloHalloHalloSimonSunneschin");

        byte[] bytes = message.getBytes();

        ManetMessage recvMessage = new ManetMessage(bytes);


        String oldMessage = message.getMessage();
        String newMessage = recvMessage.getMessage();


        assertTrue(oldMessage.equals(newMessage));

    }




}